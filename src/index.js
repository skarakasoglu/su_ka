import './styles/styles.scss';
import { headerComponent } from './components/header/HeaderComponent'
import { tabContentComponent } from './components/TabContentComponent'
import { aboutComponent } from './components/tabs/AboutComponent';
import { UserService } from './service/UserService';

const userService = new UserService();

let mainComponentState = {
    selectedTab: aboutComponent,
    selectedTabName: 'about',
}

const onSelectedTabChanged = (component, selectedTabName) => {
    document.body.innerHTML = '';

    mainComponentState = {
        ...mainComponentState,
        selectedTab: component,
        selectedTabName: selectedTabName,
    };
    document.body.appendChild(mainComponent());
}

const onUserEdit = (user) => {
    document.body.innerHTML = '';

    userService.setUser(user);
    document.body.appendChild(mainComponent());
}

const mainComponent = (props) => {
    const element = document.createElement("div");
    element.classList.add('main-container');

    const selectedUser = userService.getUser(0);

    element.appendChild(headerComponent({
        user: selectedUser,
        onSelectedTabChanged: onSelectedTabChanged,
        selectedTabName: mainComponentState.selectedTabName,
    }));
    element.appendChild(tabContentComponent({
        user: selectedUser,
        selectedTab: mainComponentState.selectedTab,
        onUserEdit: onUserEdit,
    }));

    return element;
}

document.body.appendChild(mainComponent());