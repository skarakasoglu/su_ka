import '../styles/styles.scss';

const tabContentComponent = (props) => {
    const element = document.createElement('div');
    element.classList.add('panel', 'tab-content', 'box');

    element.appendChild(props.selectedTab({
        user: props.user,
        onUserEdit: props.onUserEdit,
    }));

    return element;
}

export { tabContentComponent }