import '../../../styles/styles.scss'
import { profileSummaryComponent } from './ProfileSummaryComponent';
import { profileTabsComponent } from './ProfileTabsComponent';

const profileComponent = (props) => {
    const element = document.createElement('div');
    element.classList.add("profile");

    const profileImageContainer = document.createElement('div');
    profileImageContainer.classList.add('profile-image-container', 'box');
    const profileImage = document.createElement('img');
    profileImage.classList.add('profile-image');
    profileImage.setAttribute('src', props.user.avatarURL);

    profileImageContainer.appendChild(profileImage);

    element.appendChild(profileImageContainer);
    element.appendChild(profileSummaryComponent({
        user: props.user,
    }));
    element.appendChild(profileTabsComponent({
        user: props.user,
        onSelectedTabChanged: props.onSelectedTabChanged,
        selectedTabName: props.selectedTabName,
    }));

    return element;
}

export { profileComponent }