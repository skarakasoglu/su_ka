import '../../../styles/styles.scss';
import { aboutComponent } from '../../tabs/AboutComponent';
import { option1Component } from '../../tabs/Option1Component';
import { option2Component } from '../../tabs/Option2Component';
import { option3Component } from '../../tabs/Option3Component';
import { settingsComponent } from '../../tabs/SettingsComponent';
import { followersComponent } from './FollowersComponent';

const profileTabsComponent = (props) => {
    const element = document.createElement("div");
    element.classList.add('profile-tabs');

    const tabList = document.createElement('ul');
    tabList.classList.add('tab-nav');

    const aboutElem = document.createElement('li');
    const aboutButton = document.createElement('a');
    aboutElem.setAttribute('tag', 'about');
    aboutButton.innerHTML = "about";
    aboutButton.onclick = () => props.onSelectedTabChanged(aboutComponent, 'about');
    aboutElem.appendChild(aboutButton);

    const settingsElem = document.createElement('li');
    const settingsButton = document.createElement('a');
    settingsButton.innerHTML = "settings";
    settingsElem.setAttribute('tag', 'settings');
    settingsButton.onclick = () => props.onSelectedTabChanged(settingsComponent, 'settings');
    settingsElem.appendChild(settingsButton);

    const option1 = document.createElement('li');
    option1.setAttribute('tag', 'option1');
    const option1Button = document.createElement('a');
    option1Button.innerHTML = "option1";
    option1Button.onclick = () => props.onSelectedTabChanged(option1Component, 'option1');
    option1.appendChild(option1Button);

    const option2 = document.createElement('li');
    option2.setAttribute('tag', 'option2');
    const opiton2Button = document.createElement('a');
    opiton2Button.innerHTML = "option2";
    opiton2Button.onclick = () => props.onSelectedTabChanged(option2Component, 'option2');
    option2.appendChild(opiton2Button);

    const option3 = document.createElement('li');
    option3.setAttribute('tag', 'option3');
    const option3Button = document.createElement('a');
    option3Button.innerHTML = "option3";
    option3Button.onclick = () => props.onSelectedTabChanged(option3Component, 'option3');
    option3.appendChild(option3Button);

    const option4 = document.createElement('li');
    const option4Button = document.createElement('a');
    option4Button.innerHTML = "option4";
    option4.appendChild(option4Button);

    tabList.appendChild(aboutElem);
    tabList.appendChild(settingsElem);
    tabList.appendChild(option1);
    tabList.appendChild(option2);
    tabList.appendChild(option3);
    tabList.appendChild(option4);

    const children = [...tabList.children];
    children.forEach(child => {
        if (child.getAttribute('tag') == props.selectedTabName) {
            child.classList.add('selected');
        }
    });

    element.appendChild(tabList);
    element.appendChild(followersComponent({
        followerCount: props.user.followers,
    }));

    return element;
}

export { profileTabsComponent };