import '../../../styles/styles.scss';

const reviewsComponent = (props) => {
    const element = document.createElement('div');
    element.classList.add("profile-reviews");

    let reviewAverage = 0;

    const reviews = [...props.user.reviews];
    let reviewSum = reviews.reduce((sum, review) => {
        return review.star + sum;
    }, 0);
    reviewAverage = reviewSum / reviews.length;

    const reviewStars = document.createElement('div');
    reviewStars.classList.add('review-stars');

    const maxStarCount = 5;

    for (let i = 0; i < reviewAverage; i++) {
        const icon = document.createElement('ion-icon');
        icon.setAttribute('name', 'star');
        reviewStars.appendChild(icon);
    }

    for (let i = reviewAverage; i < maxStarCount; i++) {
        const icon = document.createElement('ion-icon');
        icon.setAttribute('name', 'star-outline');
        reviewStars.appendChild(icon);
    }

    const reviewElem = document.createElement('div');
    reviewElem.classList.add("review-count");
    reviewElem.innerHTML = `<span>${reviews.length}  Reviews</span>`;

    element.appendChild(reviewStars);
    element.appendChild(reviewElem);

    return element;
}

export { reviewsComponent };