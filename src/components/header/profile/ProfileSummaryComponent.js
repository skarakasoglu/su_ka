import '../../../styles/styles.scss';
import { profileInformationComponent } from './ProfileInformationComponent';
import { reviewsComponent } from './ReviewsComponent';

const profileSummaryComponent = (props) => {
    const element = document.createElement('div');
    element.classList.add("profile-summary");

    element.appendChild(profileInformationComponent({
        user: props.user,
    }));

    element.appendChild(reviewsComponent({
        user: props.user,
    }));

    return element;
}

export { profileSummaryComponent }