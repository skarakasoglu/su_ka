import '../../../styles/styles.scss';

const followersComponent = (props) => {
    const element = document.createElement('div');
    element.classList.add("followers");

    element.innerHTML = `
    <ion-icon name="add-circle"></ion-icon>
    <span>${props.followerCount}</span>
    Followers
                        `;
    
    return element;
}

export { followersComponent };