import '../../../styles/styles.scss';

const profileInformationComponent = (props) => {
    const element = document.createElement('div');
    element.classList.add('profile-info');

    const profileNameElement = document.createElement("span");
    profileNameElement.classList.add("profile-name");
    profileNameElement.innerHTML = props.user.name;

    const addressElement = document.createElement('span');
    addressElement.classList.add("profile-address");
    addressElement.innerHTML = '<ion-icon name="location-outline"></ion-icon>' + props.user.address;

    const phoneNumberElement = document.createElement("span");
    phoneNumberElement.classList.add("profile-phone-number");
    phoneNumberElement.innerHTML = '<ion-icon name="call-outline"></ion-icon>' + props.user.phoneNumber;

    element.appendChild(profileNameElement);
    element.appendChild(addressElement);
    element.appendChild(phoneNumberElement);

    return element;
}

export { profileInformationComponent };