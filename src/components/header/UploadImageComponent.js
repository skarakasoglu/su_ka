import '../../styles/styles.scss';

const uploadImageComponent = () => {
    const element = document.createElement('div');
    element.classList.add("upload-button-container");

    const uploadBtn = document.createElement('button');
    uploadBtn.classList.add('button', 'button--state-disabled', 'button--upload-img');


    uploadBtn.innerHTML = '<ion-icon name="camera"></ion-icon> Upload Cover Image';
    
    
    element.appendChild(uploadBtn);

    return element;
}

export { uploadImageComponent }