import '../../styles/styles.scss';
import { logoutComponent } from './LogoutComponent'
import { profileComponent } from './profile/ProfileComponent';
import { uploadImageComponent } from './UploadImageComponent'

const headerComponent = (props) => {
    const element = document.createElement('header');
    element.classList.add('panel', 'box');

    element.appendChild(logoutComponent());
    element.appendChild(uploadImageComponent());
    element.appendChild(profileComponent({
        user: props.user,
        onSelectedTabChanged: props.onSelectedTabChanged,
        selectedTabName: props.selectedTabName,
    }));

    return element;
}

export { headerComponent }