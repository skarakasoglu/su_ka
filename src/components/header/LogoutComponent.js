import '../../styles/styles.scss';

const logoutComponent = () => {
    const element = document.createElement('div');
    element.classList.add('logout-container');

    const logoutBtn = document.createElement('button');
    logoutBtn.classList.add('button', 'button--state-primary-reverse');
    logoutBtn.innerHTML = 'LOG OUT';

    element.appendChild(logoutBtn);

    return element;
}

export { logoutComponent }