import '../../../styles/styles.scss';
import { mobileEditItem } from '../../utility/MobileEditItem';

const mobileAboutEditComponent = (props) => {
    const element = document.createElement('div');
    element.classList.add('about-user-edit', 'hide');

    const titleDiv = document.createElement('div');
    titleDiv.classList.add('title');
    const title = document.createElement('span');
    title.classList.add('title');
    title.innerHTML = 'About';

    const saveBtn = document.createElement('span');
    saveBtn.classList.add('button', 'button--state-primary');
    saveBtn.innerHTML = 'save';
    saveBtn.addEventListener('click', () => {
        console.log('clicked');
    });

    const cancelBtn = document.createElement('span');
    cancelBtn.classList.add('button', 'button--state-primary');
    cancelBtn.innerHTML = 'cancel';
    cancelBtn.addEventListener('click', () => {
        const viewElem = document.querySelector('.mobile .about-user');
        viewElem.classList.remove('hide');
        element.classList.add('hide');
    });

    titleDiv.appendChild(title);
    titleDiv.appendChild(cancelBtn);
    titleDiv.appendChild(saveBtn);

    const form = document.createElement('div');
    form.classList.add('user-edit-form');

    let fullName = props.user.name.split(' ');

    const firstNameEdit = mobileEditItem({
        title: 'First Name',
        value: fullName[0],
    });

    const lastNameEdit = mobileEditItem({
        title: 'Last Name',
        value: fullName[1],
    });

    const websiteEdit = mobileEditItem({
        title: 'Website',
        value: props.user.website,
    });

    const phoneNumberEdit = mobileEditItem({
        title: 'Phone Number',
        value: props.user.phoneNumber,
    });

    const addressEdit = mobileEditItem({
        title: 'City, State & Zip',
        value: props.user.address,
    });


    saveBtn.addEventListener('click', () => {
        const findValue = editItems => {
            let result = '';
            editItems.forEach(e => {
                if (e.tagName.toLowerCase() == 'input') {
                    result =  e.value;
                }
            });

            return result;
        }


        let firstName = findValue([...firstNameEdit.children]);
        let lastName = findValue([...lastNameEdit.children]);
        let website = findValue([...websiteEdit.children]);
        let phone = findValue([...phoneNumberEdit.children]);
        let address = findValue([...addressEdit.children]);

        const user = {
            ...props.user,
            name: `${firstName} ${lastName}`,
            website: website,
            phoneNumber: phone,
            address: address,
        };
        props.onUserEdit(user);
    });

    form.appendChild(firstNameEdit);
    form.appendChild(lastNameEdit);
    form.appendChild(websiteEdit);
    form.appendChild(phoneNumberEdit)
    form.appendChild(addressEdit);

    element.appendChild(titleDiv);
    element.appendChild(form);
    return element;
}

export { mobileAboutEditComponent };