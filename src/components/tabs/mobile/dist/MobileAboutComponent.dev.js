"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mobileAboutComponent = void 0;

require("../../../styles/styles.scss");

var _MobileAboutEditComponent = require("./MobileAboutEditComponent");

var _MobileAboutViewComponent = require("./MobileAboutViewComponent");

var mobileAboutComponent = function mobileAboutComponent(props) {
  var element = document.createElement('div');
  element.classList.add('mobile');
  var viewComponent = (0, _MobileAboutViewComponent.mobileAboutViewComponent)(props);
  var editComponent = (0, _MobileAboutEditComponent.mobileAboutEditComponent)(props);
  element.appendChild(viewComponent);
  element.appendChild(editComponent);
  return element;
};

exports.mobileAboutComponent = mobileAboutComponent;