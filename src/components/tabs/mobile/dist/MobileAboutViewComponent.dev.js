"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mobileAboutViewComponent = void 0;

require("../../../styles/styles.scss");

var mobileAboutViewComponent = function mobileAboutViewComponent(props) {
  var element = document.createElement('div');
  element.classList.add('about-user');
  var titleDiv = document.createElement('div');
  titleDiv.classList.add('title');
  var title = document.createElement('span');
  title.classList.add('title');
  title.innerHTML = 'About';
  var editBtn = document.createElement('span');
  editBtn.classList.add('edit');
  editBtn.innerHTML = '<ion-icon name="pencil"></ion-icon>';
  editBtn.addEventListener('click', function () {
    var editElem = document.querySelector('div.about-user-edit');
    editElem.classList.remove('hide');
    element.classList.add('hide');
  });
  titleDiv.appendChild(title);
  titleDiv.appendChild(editBtn);
  var profileInfo = document.createElement('div');
  profileInfo.classList.add('profile-info');
  var name = document.createElement('span');
  name.classList.add('profile-name');
  name.innerHTML = props.user.name;
  var website = document.createElement('span');
  website.innerHTML = "<span><ion-icon name=\"globe-outline\"></ion-icon> ".concat(props.user.website, "</span>");
  var phone = document.createElement('span');
  phone.innerHTML = "<span><ion-icon name=\"call-outline\"></ion-icon> ".concat(props.user.phoneNumber, "</span>");
  var address = document.createElement('span');
  address.innerHTML = "<span><ion-icon name=\"home-outline\"></ion-icon> ".concat(props.user.address, "</span>");
  profileInfo.appendChild(name);
  profileInfo.appendChild(website);
  profileInfo.appendChild(phone);
  profileInfo.appendChild(address);
  element.appendChild(titleDiv);
  element.appendChild(profileInfo);
  return element;
};

exports.mobileAboutViewComponent = mobileAboutViewComponent;