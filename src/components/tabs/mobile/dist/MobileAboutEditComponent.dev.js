"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mobileAboutEditComponent = void 0;

require("../../../styles/styles.scss");

var _MobileEditItem = require("../../utility/MobileEditItem");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var mobileAboutEditComponent = function mobileAboutEditComponent(props) {
  var element = document.createElement('div');
  element.classList.add('about-user-edit', 'hide');
  var titleDiv = document.createElement('div');
  titleDiv.classList.add('title');
  var title = document.createElement('span');
  title.classList.add('title');
  title.innerHTML = 'About';
  var saveBtn = document.createElement('span');
  saveBtn.classList.add('button', 'button--state-primary');
  saveBtn.innerHTML = 'save';
  saveBtn.addEventListener('click', function () {
    console.log('clicked');
  });
  var cancelBtn = document.createElement('span');
  cancelBtn.classList.add('button', 'button--state-primary');
  cancelBtn.innerHTML = 'cancel';
  cancelBtn.addEventListener('click', function () {
    var viewElem = document.querySelector('.mobile .about-user');
    viewElem.classList.remove('hide');
    element.classList.add('hide');
  });
  titleDiv.appendChild(title);
  titleDiv.appendChild(cancelBtn);
  titleDiv.appendChild(saveBtn);
  var form = document.createElement('div');
  form.classList.add('user-edit-form');
  var fullName = props.user.name.split(' ');
  var firstNameEdit = (0, _MobileEditItem.mobileEditItem)({
    title: 'First Name',
    value: fullName[0]
  });
  var lastNameEdit = (0, _MobileEditItem.mobileEditItem)({
    title: 'Last Name',
    value: fullName[1]
  });
  var websiteEdit = (0, _MobileEditItem.mobileEditItem)({
    title: 'Website',
    value: props.user.website
  });
  var phoneNumberEdit = (0, _MobileEditItem.mobileEditItem)({
    title: 'Phone Number',
    value: props.user.phoneNumber
  });
  var addressEdit = (0, _MobileEditItem.mobileEditItem)({
    title: 'City, State & Zip',
    value: props.user.address
  });
  saveBtn.addEventListener('click', function () {
    var findValue = function findValue(editItems) {
      var result = '';
      editItems.forEach(function (e) {
        if (e.tagName.toLowerCase() == 'input') {
          result = e.value;
        }
      });
      return result;
    };

    var firstName = findValue(_toConsumableArray(firstNameEdit.children));
    var lastName = findValue(_toConsumableArray(lastNameEdit.children));
    var website = findValue(_toConsumableArray(websiteEdit.children));
    var phone = findValue(_toConsumableArray(phoneNumberEdit.children));
    var address = findValue(_toConsumableArray(addressEdit.children));

    var user = _objectSpread({}, props.user, {
      name: "".concat(firstName, " ").concat(lastName),
      website: website,
      phoneNumber: phone,
      address: address
    });

    props.onUserEdit(user);
  });
  form.appendChild(firstNameEdit);
  form.appendChild(lastNameEdit);
  form.appendChild(websiteEdit);
  form.appendChild(phoneNumberEdit);
  form.appendChild(addressEdit);
  element.appendChild(titleDiv);
  element.appendChild(form);
  return element;
};

exports.mobileAboutEditComponent = mobileAboutEditComponent;