import '../../../styles/styles.scss';
import { mobileAboutEditComponent } from './MobileAboutEditComponent';
import { mobileAboutViewComponent } from './MobileAboutViewComponent';

const mobileAboutComponent = (props) => {
    const element = document.createElement('div');
    element.classList.add('mobile');

    const viewComponent = mobileAboutViewComponent(props);
    const editComponent = mobileAboutEditComponent(props);

    element.appendChild(viewComponent);
    element.appendChild(editComponent);

    return element;
}

export { mobileAboutComponent };