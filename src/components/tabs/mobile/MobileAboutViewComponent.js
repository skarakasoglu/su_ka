import '../../../styles/styles.scss';

const mobileAboutViewComponent = (props) => {
    const element = document.createElement('div');
    element.classList.add('about-user');

    const titleDiv = document.createElement('div');
    titleDiv.classList.add('title');
    const title = document.createElement('span');
    title.classList.add('title');
    title.innerHTML = 'About';

    const editBtn = document.createElement('span');
    editBtn.classList.add('edit');
    editBtn.innerHTML = '<ion-icon name="pencil"></ion-icon>';
    editBtn.addEventListener('click', () => {
        const editElem = document.querySelector('div.about-user-edit');
        editElem.classList.remove('hide');
        element.classList.add('hide');
    });

    titleDiv.appendChild(title);
    titleDiv.appendChild(editBtn);

    const profileInfo = document.createElement('div');
    profileInfo.classList.add('profile-info');

    const name = document.createElement('span');
    name.classList.add('profile-name');
    name.innerHTML = props.user.name;

    const website = document.createElement('span');
    website.innerHTML = `<span><ion-icon name="globe-outline"></ion-icon> ${props.user.website}</span>`;

    const phone = document.createElement('span');
    phone.innerHTML = `<span><ion-icon name="call-outline"></ion-icon> ${props.user.phoneNumber}</span>`;

    const address = document.createElement('span');
    address.innerHTML = `<span><ion-icon name="home-outline"></ion-icon> ${props.user.address}</span>`;

    profileInfo.appendChild(name);
    profileInfo.appendChild(website);
    profileInfo.appendChild(phone);
    profileInfo.appendChild(address);

    element.appendChild(titleDiv);
    element.appendChild(profileInfo);
    return element;
}

export { mobileAboutViewComponent };