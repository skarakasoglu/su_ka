import '../../styles/styles.scss';
import { desktopAboutComponent } from './desktop/DesktopAboutComponent';
import { mobileAboutComponent } from './mobile/MobileAboutComponent';

const aboutComponent = (props) => {
    const element = document.createElement('div');
    element.appendChild(desktopAboutComponent(props));
    element.appendChild(mobileAboutComponent(props));

    return element;
}

export { aboutComponent };