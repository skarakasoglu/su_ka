"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.desktopAboutComponent = void 0;

require("../../../styles/styles.scss");

var _EditButtonComponent = require("../../utility/EditButtonComponent");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var desktopAboutComponent = function desktopAboutComponent(props) {
  var element = document.createElement('div');
  element.classList.add("about-user", 'desktop');
  var title = document.createElement("div");
  title.classList.add('title');
  title.innerHTML = '<span class="title">About</span>';
  var name = document.createElement('div');
  name.classList.add("profile-name");
  name.innerHTML = "<span>".concat(props.user.name, "</span>");
  name.appendChild((0, _EditButtonComponent.editButtonComponent)({
    title: 'Name',
    content: props.user.name,
    onSave: function onSave(name) {
      var user = _objectSpread({}, props.user);

      user.name = name;
      props.onUserEdit(user);
    }
  }));
  var website = document.createElement('div');
  website.innerHTML = "<span><ion-icon name=\"globe-outline\"></ion-icon> ".concat(props.user.website, "</span>");
  website.appendChild((0, _EditButtonComponent.editButtonComponent)({
    title: 'Website',
    content: props.user.website,
    onSave: function onSave(website) {
      var user = _objectSpread({}, props.user);

      user.website = website;
      props.onUserEdit(user);
    }
  }));
  var phone = document.createElement('div');
  phone.innerHTML = "<span><ion-icon name=\"call-outline\"></ion-icon> ".concat(props.user.phoneNumber, "</span>");
  phone.appendChild((0, _EditButtonComponent.editButtonComponent)({
    title: 'Phone Number',
    content: props.user.phoneNumber,
    onSave: function onSave(phoneNumber) {
      var user = _objectSpread({}, props.user);

      user.phoneNumber = phoneNumber;
      props.onUserEdit(user);
    }
  }));
  var address = document.createElement('div');
  address.innerHTML = "<span><ion-icon name=\"home-outline\"></ion-icon> ".concat(props.user.address, "</span>");
  address.appendChild((0, _EditButtonComponent.editButtonComponent)({
    title: 'City, State & Zip',
    content: props.user.address,
    onSave: function onSave(address) {
      var user = _objectSpread({}, props.user);

      user.address = address;
      props.onUserEdit(user);
    }
  }));
  element.appendChild(title);
  element.appendChild(name);
  element.appendChild(website);
  element.appendChild(phone);
  element.appendChild(address);
  return element;
};

exports.desktopAboutComponent = desktopAboutComponent;