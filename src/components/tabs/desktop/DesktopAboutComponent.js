import '../../../styles/styles.scss';
import { editButtonComponent } from '../../utility/EditButtonComponent';

const desktopAboutComponent = (props) => {
    const element = document.createElement('div');
    element.classList.add("about-user", 'desktop');
    
    const title = document.createElement("div");
    title.classList.add('title');
    title.innerHTML = '<span class="title">About</span>';

    const name = document.createElement('div');
    name.classList.add("profile-name");
    name.innerHTML = `<span>${props.user.name}</span>`;
    name.appendChild(editButtonComponent({
        title: 'Name',
        content: props.user.name,
        onSave: (name) => {
            const user = {...props.user};
            user.name = name;
            props.onUserEdit(user);
        },    
    }));


    const website = document.createElement('div');
    website.innerHTML = `<span><ion-icon name="globe-outline"></ion-icon> ${props.user.website}</span>`;
    website.appendChild(editButtonComponent({
        title: 'Website',
        content: props.user.website,
        onSave: (website) => {
            const user = {...props.user};
            user.website = website;
            props.onUserEdit(user);
        },
    }));
    

    const phone = document.createElement('div');
    phone.innerHTML = `<span><ion-icon name="call-outline"></ion-icon> ${props.user.phoneNumber}</span>`;
    phone.appendChild(editButtonComponent({
        title: 'Phone Number',
        content: props.user.phoneNumber,
        onSave: (phoneNumber) => {
            const user = {...props.user};
            user.phoneNumber = phoneNumber;
            props.onUserEdit(user);
        }
    }));

    const address = document.createElement('div');
    address.innerHTML = `<span><ion-icon name="home-outline"></ion-icon> ${props.user.address}</span>`;
    address.appendChild(editButtonComponent({
        title: 'City, State & Zip',
        content: props.user.address,
        onSave: (address) => {
            const user = {...props.user};
            user.address = address;
            props.onUserEdit(user);
        }
    }));

    element.appendChild(title);
    element.appendChild(name);
    element.appendChild(website);
    element.appendChild(phone);
    element.appendChild(address);

    return element;
}

export { desktopAboutComponent };