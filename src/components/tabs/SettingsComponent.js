import '../../styles/styles.scss';

const settingsComponent = () => {
    const element = document.createElement('div');
    element.innerHTML = 'Settings';

    return element;
}

export { settingsComponent };