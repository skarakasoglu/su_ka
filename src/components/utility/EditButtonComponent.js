import '../../styles/styles.scss';
import { popoverComponent } from './PopoverComponent';

const editButtonComponent = (props) => {
    const element = document.createElement('span');
    element.classList.add('edit');

    const elementBtn = document.createElement('span');
    elementBtn.innerHTML = '<ion-icon name="pencil"></ion-icon>';
    elementBtn.addEventListener('click', () => {
        element.classList.add('active');
    });

    const elementPopover = popoverComponent({
        ...props,
        editButton: element,
    });

    element.appendChild(elementBtn);
    element.appendChild(elementPopover);

    return element;
}

export { editButtonComponent };