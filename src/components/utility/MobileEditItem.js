import '../../styles/styles.scss';

const mobileEditItem = (props) => {
    const element = document.createElement('div');
    element.classList.add('edit-item');

    const title = document.createElement('span');
    title.innerHTML = props.title;

    const value = document.createElement('input');
    value.setAttribute('type', 'text');
    value.setAttribute('value', props.value);

    element.appendChild(title);
    element.appendChild(value);

    return element;
}

export { mobileEditItem };