import '../../styles/styles.scss';

const popoverComponent = (props) => {
    const element = document.createElement('div');
    element.classList.add('popover-body', 'box');

    const popoverTitle = document.createElement('div');
    popoverTitle.classList.add('popover-title');
    popoverTitle.innerHTML = `<span>${props.title}</span>`;

    const popoverContent = document.createElement('div');
    popoverContent.classList.add('popover-content');

    const contentInput = document.createElement('input');
    contentInput.setAttribute('value', props.content);
    contentInput.setAttribute('type', 'text');
    popoverContent.appendChild(contentInput);

    const popoverButtons = document.createElement('div');
    popoverButtons.classList.add('popover-buttons');

    const save = document.createElement('button');
    save.classList.add('button', 'button--state-primary');
    save.innerHTML = 'save';
    save.addEventListener('click', () => props.onSave(contentInput.value));

    const cancel = document.createElement('button');
    cancel.classList.add('button', 'button--state-primary-reverse');
    cancel.innerHTML = 'cancel';
    cancel.addEventListener('click', () => {
        props.editButton.classList.remove('active');
    });

    popoverButtons.appendChild(save);
    popoverButtons.appendChild(cancel);

    element.appendChild(popoverTitle);
    element.appendChild(popoverContent);
    element.appendChild(popoverButtons);

    return element;
}

export { popoverComponent };