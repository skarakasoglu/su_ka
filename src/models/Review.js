export class Review{
    constructor(star, comment) {
        this.star = star;
        this.comment = comment;
    }
}