import { add } from "lodash";

export class User{
    constructor(name, website, address, phoneNumber, avatarURL, reviews, followers) {
        this.name = name;
        this.website = website;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.avatarURL = avatarURL;
        this.reviews = reviews;
        this.followers = followers;
    }
}