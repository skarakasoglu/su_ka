import { Review } from "../models/Review";
import { User } from "../models/User"


class UserService {
    constructor() {
        this.users = [new User("Jessica Parker", "www.seller.com", "Newport Beach, CA", "(949) 325 - 68594", 'images/profile_image.jpg', [
            new Review(4, "Test comment"),
            new Review(4, "Test comment"),
            new Review(4, "Test comment"),
            new Review(4, "Test comment"),
            new Review(4, "Test comment"),
            new Review(4, "Test comment"),
        ], 15)];
    }

    getUser(id) {
        return this.users[id];
    }

    setUser(userP) {
        const user = this.users[0];
        user.name = userP.name;
        user.website = userP.website;
        user.address = userP.address;
        user.phoneNumber = userP.phoneNumber;
    }
}

export { UserService };